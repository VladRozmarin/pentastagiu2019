import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class FileReader {

	public String readFromFile(String fileName) {
		StringBuilder text = new StringBuilder();

		{
			// read from file from line to line
			try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

				stream.forEach(l -> {
					text.append(l);
					// new line character
					text.append("\n");
				});

			} catch (IOException e) {
				e.printStackTrace();
				// catch exception and print stack trace
			}
			return text.toString();
		}

	}
}
